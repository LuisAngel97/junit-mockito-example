package com.nova.solutions.junit.mockito.example.junit.util;

import com.nova.solutions.junit.mockito.example.util.Util;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import java.math.BigDecimal;

@DisplayName("Asserts in JUnit 5.")
@TestInstance(Lifecycle.PER_METHOD)
public class JunitAsserts {

}
